package R12_14;

/**
 * Created by kld on 2014/11/9.
 */
public class country {
    	/*
	name is the name of the country
	area is the area of the country
	population is the population of the country
	*/

    private String name;
    private int area;
    private int population;
    private double populationDensity;

    /*
    get the name of a country
    */
    public String getName(){
        return name;
    }

    /*
    get the area of a country
    */
    public int getArea(){
        return area;
    }

    /*
    get the population of a country
    */
    public int getPopulation(){
        return population;
    }

    public double getPopulationDensity(){
        return populationDensity;
    }
}
