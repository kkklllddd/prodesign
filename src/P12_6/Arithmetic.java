package P12_6;

import java.util.Scanner;

/**
 * Created by kld on 2014/11/7.
 * Write a program that teaches arithmetic to a young child. The program tests addition
 and subtraction. In level 1, it tests only addition of numbers less than 10 whose sum
 is less than 10. In level 2, it tests addition of arbitrary one-digit numbers. In level 3, it
 tests subtraction of one-digit numbers with a nonnegative difference.
 Generate random problems and get the player’s input. The player gets up to two
 tries per problem. Advance from one level to the next when the player has achieved a
 score of five points.
 */
public class Arithmetic {

    private static int score;
    private static boolean answer;

    public static void main(String[] args) {
        System.out.println("Level 1");
        level1();
        System.out.println("Level 2");
        level2();
        System.out.println("Level 3");
        level3();
        System.out.println("Good job!");
    }

    private static void level1(){
        score = 0;
        while (score < 5){
            if(level1_exercise())score++;
        }
        System.out.println("Pass level 1.");
    }

    private static boolean level1_exercise(){
        int a = (int)(Math.random() * 10);
        int b = (int)(Math.random() * 10);
        int c = a + b;
        while (c>=10){
            a = (int)(Math.random() * 10);
            b = (int)(Math.random() * 10);
            c = a + b;
        }
        System.out.print(a + " + " + b + " = ");
        Scanner in = new Scanner(System.in);
        String answer = in.nextLine();
        if(answer.compareTo(Integer.toString(c))==0){
            System.out.println("Correct.");
            return true;
        }
        else {
            System.out.print("Error, please answer again:\n" + a + " + " + b + " = ");
            answer = in.nextLine();
            if(answer.compareTo(Integer.toString(c))==0){
                System.out.println("Correct.");
                return true;
            }
            else {
                System.out.println("Sorry,incorrect again!");
                return false;
            }
        }
    }

    private static void level2(){
        score = 0;
        while (score < 5){
            if(level2_exercise())score++;
        }
        System.out.println("Pass level 2.");
    }

    private static boolean level2_exercise(){
        int a = (int)(Math.random() * 10);
        int b = (int)(Math.random() * 10);
        int c = a + b;
        System.out.print(a + " + " + b + " = ");
        Scanner in = new Scanner(System.in);
        String answer = in.nextLine();
        if(answer.compareTo(Integer.toString(c))==0){
            System.out.println("Correct.");
            return true;
        }
        else {
            System.out.print("Error, please answer again:\n" + a + " + " + b + " = ");
            answer = in.nextLine();
            if(answer.compareTo(Integer.toString(c))==0){
                System.out.println("Correct.");
                return true;
            }
            else{
                System.out.println("Sorry,incorrect again!");
                return false;
            }
        }
    }

    private static void level3(){
        score = 0;
        while (score < 5){
            if(level3_exercise())score++;
        }
        System.out.println("Pass level 3.");

    }

    private static boolean level3_exercise(){
        int a = (int)(Math.random() * 1000);
        int b = (int)(Math.random() * 10);
        int c = a - b;
        System.out.print(a + " - " + b + " = ");
        Scanner in = new Scanner(System.in);
        String answer = in.nextLine();
        if(answer.compareTo(Integer.toString(c))==0){
            System.out.println("Correct.");
            return true;
        }
        else {
            System.out.print("Error, please answer again:\n" + a + " - " + b + " = ");
            answer = in.nextLine();
            if(answer.compareTo(Integer.toString(c))==0){
                System.out.println("Correct.");
                return true;
            }
            else{
                System.out.println("Sorry,incorrect again!");
                return false;
            }
        }
    }
}
