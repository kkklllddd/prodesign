package R12_18;

import java.util.ArrayList;

/**
 * Created by kld on 2014/11/9.
 */
public class Invoice {
    private Customer customer;
    private ArrayList<Product> products;
    private double payment;

    public void add_product(Product product){
        products.add(product);
    }

    public double getPayment(){
        return payment;
    }
}
