package R12_18;

/**
 * Created by kld on 2014/11/9.
 */
public class Address {

    private String BillAddress;
    private String ShippingAddress;

    public String getBillAddress(){
        return BillAddress;
    }

    public String getShippingAddress(){
        return ShippingAddress;
    }

    public void setBillAddress(String address){
        BillAddress = address;
    }

    public void setShippingAddress(String address){
        ShippingAddress = address;
    }
}
