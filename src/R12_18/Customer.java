package R12_18;

/**
 * Created by kld on 2014/11/9.
 */
public class Customer {
    private Address address;

    public void setAddress(Address address){
        this.address = address;
    }

    public Address getAddress(){
        return address;
    }

}
