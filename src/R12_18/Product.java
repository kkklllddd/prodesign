package R12_18;

/**
 * Created by kld on 2014/11/9.
 */
public class Product {

    private double price;
    private String name;

    public double getPrice(){
        return price;
    }

    public String getName(){
        return name;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public void setName(String name){
        this.name = name;
    }
}
