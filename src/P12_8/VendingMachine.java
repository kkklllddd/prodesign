package P12_8;

/**
 * Created by kld on 2014/11/7.
 */
public class VendingMachine {

    private product[] products = new product[10];


    public VendingMachine(){

        products[0] = new product("item_1", 2.5, 5);
        products[1] = new product("item_2", 3.5, 5);
        products[2] = new product("item_3", 4, 5);
        products[3] = new product("item_4", 4.5, 5);
        products[4] = new product("item_5", 5.5, 5);
        products[5] = new product("item_6", 6, 5);
        products[6] = new product("item_7", 7.5, 5);
        products[7] = new product("item_8", 5, 5);
        products[8] = new product("item_9", 3, 5);
        products[9] = new product("item_10", 8, 5);
    }

    public String restore(int itemIndex, int number){
        products[itemIndex].add(number);
        double money = number * products[itemIndex].getPrice();
        return (money) + "$ can be removed.";
    }

     public String buy(int itemIndex, int number, double money){
         if(products[itemIndex].getNumber()>= number && money >= number * products[itemIndex].getPrice()){
             return ("You get " + number + " " + products[itemIndex].getName() + ".");
         }
         else return ("Your money are returned.");
    }

    public String showItem(){
        String str = "";
        for (int i = 0; i <products.length ; i++) {
            str = str + (products[i].getName() + " " +
                    products[i].getPrice() + " " + products[i].getNumber() + "\n");
        }
        return str;
    }
}
