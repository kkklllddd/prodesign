package P12_8;

import java.util.Scanner;

/**
 * Created by kld on 2014/11/7.
 * Write a program that simulates a vending machine. Products can be purchased by
 inserting coins with a value at least equal to the cost of the product. A user selects a
 product from a list of available products, adds coins, and either gets the product or
 gets the coins returned. The coins are returned if insufficient money was supplied
 or if the product is sold out. The machine does not give change if too much money
 was added. Products can be restocked and money removed by an operator. Follow
 the design process that was described in this chapter. Your solution should include a
 class VendingMachine that is not coupled with the Scanner or PrintStream classes
 */
public class Driver {
    public static void main(String[] args) {

        VendingMachine machine = new VendingMachine();
        int itemIndex;
        int number;
        double money;

        System.out.println(machine.showItem());
        System.out.println("Enter 1 for purchase, " +
                "Enter 2 for restore, " +
                "Enter 0 for quit.");
        Scanner in = new Scanner(System.in);
        int enter = in.nextInt();
        while (enter != 0){
            if(enter == 1) {
                System.out.println("Enter item index:");
                itemIndex = in.nextInt()-1;
                System.out.println("Enter number:");
                number = in.nextInt();
                System.out.println("Enter money:");
                money = in.nextInt();
                System.out.println(machine.buy(itemIndex, number, money));
                enter = 5;
            }
            else if(enter == 2){
                System.out.println("Enter item index:");
                itemIndex = in.nextInt()-1;
                System.out.println("Enter number:");
                number = in.nextInt();
                System.out.println(machine.restore(itemIndex, number));
                enter = 5;
            }
            else {
                System.out.println("Enter 1 for purchase, " +
                        "Enter 2 for restore, " +
                        "Enter 0 for quit.");
                enter = in.nextInt();
            }
        }
        System.out.println("Thanks for using!");
    }
}
