package P12_8;

/**
 * Created by kld on 2014/11/7.
 */
public class product {
    private String name;
    private double price;
    private int number;

    public product(String pName, double pPrice, int pNumber){
        name = pName;
        price = pPrice;
        number = pNumber;
    }

    public String getName(){
        return name;
    }

    public double getPrice(){
        return price;
    }

    public int getNumber(){
        return number;
    }

    public void add(int number){
        this.number = this.number + number;
    }
}
